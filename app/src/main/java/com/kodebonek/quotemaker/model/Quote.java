package com.kodebonek.quotemaker.model;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.kodebonek.quotemaker.R;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.utils.Typefaces;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Quote implements Cloneable {

    private static final String TAG = "QuoteClass" ;

    private static final String SAVES_DIRECTORY = "saves";
    public static final String THUMB_DIRECTORY = "thumb";   //accessed by share module
    private static final String BG_DIRECTORY = "background";

    private static final String KEY_TEXT = "text";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_SIZE_SP = "size_sp";
    private static final String KEY_COLOR = "color" ;
    private static final String KEY_FONT = "font";
    private static final String KEY_ALIGNMENT = "alignment";
    private static final String KEY_LINEHEIGHT = "lineheight";
    private static final String KEY_THUMBNAIL = "thumbnail";
    private static final String KEY_BACKGROUND = "background";
    private static final String KEY_LEFT = "left" ;
    private static final String KEY_TOP = "top" ;

    private Context mContext;
    private HashMap<String,Object> mHashMap;
    private String mQuoteID;

    private EditText etQuote = null;
    private EditText etAuthor = null;

    private Quote(Context context,String id) {
        this.mContext = context;
        this.mHashMap = new HashMap<String,Object>();
        this.mQuoteID = id;
    }

    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setQuoteWidget(EditText etQuote) { this.etQuote = etQuote; }
    public void setAuthorWidget(EditText etAuthor) { this.etAuthor = etAuthor; }

    private void setHashMap(HashMap<String,Object> hMap) {
        mHashMap.clear();

        for (HashMap.Entry<String, Object> entry : hMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            mHashMap.put(key,value);
        }
    }

    public static ArrayList<Quote> getQuotes(Context ctx,int start,int count) {
        //File mydir = ctx.getDir(SAVES_DIRECTORY, ctx.MODE_PRIVATE);

        //init some dir
        File mydir = new File(ctx.getFilesDir().toString()+File.separator+THUMB_DIRECTORY);
        mydir.mkdirs();
        mydir = new File(ctx.getFilesDir().toString()+File.separator+BG_DIRECTORY);
        mydir.mkdirs();

        mydir = new File(ctx.getFilesDir().toString()+File.separator+SAVES_DIRECTORY);
        mydir.mkdirs();
        //File lister = mydir.getAbsoluteFile();
        File flist[] = mydir.listFiles();

        Log.d(TAG, "found "+flist.length+" saved files");
        if (flist.length==0) return null;

        ArrayList<Quote> quotes = new ArrayList<Quote>();
        int c = 0;
        for (File file : flist) {
            if (file.isFile()) {
                String filename = file.getName();
                if (filename.toLowerCase().endsWith(".txt")) {
                    String quoteID = filename.replaceFirst("[.][^.]+$", "");    //remove extension
                    Quote q = Quote.newQuote(ctx,quoteID);
                    quotes.add(q);

                    Log.d(TAG,"quoteID : "+q.getQuoteID());
                    c++;
                    if (c>=count) break;
                }
            } else if (file.isDirectory()) {

                //skip
            }
        }
/*
        Log.d(TAG, "found "+lister.list().length+" saved files");
        if (lister.list().length==0) return null;
        ArrayList<Quote> quotes = new ArrayList<Quote>();
        int c = 0;
        for (int i=start; i<lister.list().length; i++) {
            String file = lister.list()[i];
            if (file.toLowerCase().endsWith(".txt")) {
                String quoteID = file.replaceFirst("[.][^.]+$", "");    //remove extension
                Quote q = Quote.newQuote(ctx,quoteID);
                quotes.add(q);

                Log.d(TAG,"quoteID : "+q.getQuoteID());
                c++;
                if (c>=count) break;
            }
        }
*/

        if (quotes.size()==0) return null;
        return quotes;
    }

    public String getQuoteID() { return mQuoteID; }
    public Uri getThumbnail() {
        String path = (String) mHashMap.get(KEY_THUMBNAIL);
        if (path==null) {
            Log.e(TAG,"unable to get thumbnail");
            return null;
        }

        Uri thumbUri = Uri.fromFile(new File(path));
        File file = new File(thumbUri.getPath());
        if (file.exists()==false) return null;
        return thumbUri;
    }
    public Uri getBackground() {
        String path = (String) mHashMap.get(KEY_BACKGROUND);
        if (path==null) {
            Log.e(TAG,"unable to get background");
            return null;
        }

        Uri bgUri = Uri.fromFile(new File(path));
        File file = new File(bgUri.getPath());
        if (file.exists()==false) return null;
        return bgUri;
    }

    public static Quote newQuote(Context ctx,String quoteid) {
        HashMap<String,Object> hashMap;
        Log.d(TAG,"load quote id:"+quoteid);
        try
        {
            String filename = quoteid+".txt";
            //File file = new File(ctx.getDir(SAVES_DIRECTORY, ctx.MODE_PRIVATE), filename);
            File file = new File(ctx.getFilesDir().toString()+File.separator+SAVES_DIRECTORY, filename);
            if (file==null) {
                Log.e(TAG,"file "+file.getPath()+" not found!");
                return null;
            }
            FileInputStream fileIn = new FileInputStream(file.getAbsoluteFile());
            ObjectInputStream objIn = new ObjectInputStream(fileIn);
            hashMap = (HashMap) objIn.readObject();
            objIn.close();
            fileIn.close();

            Quote quote = new Quote(ctx,quoteid);
            quote.setHashMap(hashMap);
            return quote;

        } catch(Exception e) {
            Log.e(TAG,"newQuote error: "+e.getMessage());
            return null;
        }
    }

    public static Quote newQuote(Context ctx) {
        try
        {
            //create our new quote from fresh
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
            Calendar cal = Calendar.getInstance();
            String quoteID = dateFormat.format(cal.getTime());

            Log.d(TAG, "create new quote id:" + quoteID);

            Quote quote = new Quote(ctx,quoteID);
            return quote;

        } catch(Exception e) {
            Log.e(TAG,"newQuote error: "+e.getMessage());
            return null;
        }
    }

    public boolean deleteQuoteFiles() {
        try
        {
            String image_file = mQuoteID+ ".png";
            File fileThumb = new File(mContext.getFilesDir().toString()+File.separator+Quote.THUMB_DIRECTORY, image_file);
            fileThumb.delete();

            image_file = mQuoteID+ ".png";
            File fileBg = new File(mContext.getFilesDir().toString()+File.separator+Quote.BG_DIRECTORY, image_file);
            //File fileBg = new File(mContext.getDir(BG_DIRECTORY, mContext.MODE_PRIVATE), image_file);
            fileBg.delete();

            String filename = mQuoteID+".txt";
            File fileTxt = new File(mContext.getFilesDir().toString()+File.separator+Quote.SAVES_DIRECTORY, filename);
            //File fileTxt = new File(mContext.getDir(SAVES_DIRECTORY, mContext.MODE_PRIVATE), filename);
            fileTxt.delete();

            Log.d(TAG,"quote files deleted.");
            return true;

        } catch(Exception e) {
            Log.e(TAG,"deleteQuote error: "+e.getMessage());
            return false;
        }
    }

    public boolean saveToFile(int left,int top,Bitmap quoteBitmap,Bitmap bgBitmap) {

        String image_file = mQuoteID+ ".png";
        //File fileThumb = new File(mContext.getDir(THUMB_DIRECTORY, mContext.MODE_PRIVATE), image_file);
        File fileThumb = new File(mContext.getFilesDir().toString()+File.separator+THUMB_DIRECTORY, image_file);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(fileThumb);
            quoteBitmap.compress(Bitmap.CompressFormat.PNG, 80, out);
            out.flush();
            out.close();
            Log.d(TAG,"thumbnail saved : "+fileThumb.getPath());
        } catch (Exception e) {
            Log.e(TAG,"failed saving thumbnail to file: "+e.getMessage());
            return false;
        }

        image_file = mQuoteID+ ".png";
        //File fileBg = new File(mContext.getDir(BG_DIRECTORY, mContext.MODE_PRIVATE), image_file);
        File fileBg = new File(mContext.getFilesDir().toString()+File.separator+BG_DIRECTORY, image_file);
        out = null;
        try {
            out = new FileOutputStream(fileBg);
            bgBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            Log.d(TAG, "background saved : " + fileBg.getPath());
        } catch (Exception e) {
            Log.e(TAG,"failed saving bg to file: "+e.getMessage());
            return false;
        }

        mHashMap.put(KEY_LEFT, left);
        mHashMap.put(KEY_TOP, top);

        mHashMap.put(KEY_TEXT, etQuote.getText().toString());
        mHashMap.put(KEY_AUTHOR, etAuthor.getText().toString());

        mHashMap.put(KEY_THUMBNAIL, fileThumb.getPath());
        mHashMap.put(KEY_BACKGROUND, fileBg.getPath());

        String filename = mQuoteID+".txt";
        //File file = new File(mContext.getDir(SAVES_DIRECTORY, mContext.MODE_PRIVATE), filename);
        File file = new File(mContext.getFilesDir().toString()+File.separator+SAVES_DIRECTORY, filename);
        try
        {
            FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
            ObjectOutputStream objout = new ObjectOutputStream(fileOut);
            objout.writeObject(mHashMap);
            objout.close();
            fileOut.close();

            Log.d(TAG,"quote metadata saved to: "+file.getPath());
            return true;

        } catch(IOException e) {
            Log.e(TAG,"save quote error: "+e.getMessage());
            return false;
        }
    }

    public void setQuote(String quote) {
        mHashMap.put(KEY_TEXT, quote);
        if (etQuote!=null) etQuote.setText(quote);
    }
    public void setAuthor(String s) {
        mHashMap.put(KEY_AUTHOR, s);
        if (etAuthor!=null) etAuthor.setText(s);
    }
    public void setTextSize(int v) {
        mHashMap.put(KEY_SIZE_SP,v);
        if (etQuote!=null) etQuote.setTextSize(TypedValue.COMPLEX_UNIT_SP, v);
        if (etAuthor!=null) etAuthor.setTextSize(TypedValue.COMPLEX_UNIT_SP, v / 2);  //half of it
    }

    public void setColor(int c) {
        mHashMap.put(KEY_COLOR,c);
        if (etQuote!=null) etQuote.setTextColor(c);
        if (etAuthor!=null) etAuthor.setTextColor(c);
    }

    public void setFont(String font) {
        mHashMap.put(KEY_FONT,font);

        Typeface face = Typefaces.get(mContext,font);
        if (etQuote!=null) etQuote.setTypeface(face);
        if (etAuthor!=null) etAuthor.setTypeface(face);
    }

    public void setAlignment(int gravity) {
        mHashMap.put(KEY_ALIGNMENT,gravity);
        int width = 0;
        if (etQuote!=null) {
            etQuote.setGravity(gravity);
            width = etQuote.getWidth();
        }
        if (etAuthor!=null) {
            etAuthor.setWidth(width);
            etAuthor.setGravity(gravity);
        }
    }
    public void setLineHeight(float lineHeight) {
        mHashMap.put(KEY_LINEHEIGHT,lineHeight);
        if (etQuote!=null) etQuote.setLineSpacing(0, lineHeight);
    }

    public void renderWidget() {
        if (etQuote==null || etAuthor==null) return;

        String quote = (String) mHashMap.get(KEY_TEXT);
        if (quote==null || quote.length()==0) quote = mContext.getResources().getString(R.string.default_quote);
        setQuote(quote);

        String author = (String) mHashMap.get(KEY_AUTHOR);
        //author can be empty
        if (author==null) author = mContext.getResources().getString(R.string.default_author);
        setAuthor(author);

        Integer size = (Integer) mHashMap.get(KEY_SIZE_SP);
        if (size==null) size = mContext.getResources().getInteger(R.integer.default_text_size);
        setTextSize(size);

        Integer color = (Integer) mHashMap.get(KEY_COLOR);
        if (color==null) color = Color.WHITE;
        setColor(color);

        String font = (String) mHashMap.get(KEY_FONT);
        if (font==null) font = mContext.getResources().getString(R.string.default_font);
        setFont(font);

        Integer alignment = (Integer) mHashMap.get(KEY_ALIGNMENT);
        if (alignment==null) alignment = Gravity.LEFT;
        setAlignment(alignment);

        Float lineheight = (Float) mHashMap.get(KEY_LINEHEIGHT);
        if (lineheight!=null) setLineHeight(lineheight);

        Integer left = (Integer) mHashMap.get(KEY_LEFT);
        if (left==null) left = 50;
        Integer top = (Integer) mHashMap.get(KEY_TOP);
        if (top==null) top = 150;

        int deviceWidth = Helper.getDeviceWidth(mContext);
        left = deviceWidth + left;
        top = deviceWidth + top;
        //width = deviceWidth - 50 to make space
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(deviceWidth - 50, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(left, top, 0, 0);
        etQuote.setLayoutParams(params);

        //etAuthor.
        RelativeLayout.LayoutParams authorParams = new RelativeLayout.LayoutParams(deviceWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        authorParams.addRule(RelativeLayout.BELOW, R.id.etQuote);
        authorParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.etQuote);
        authorParams.addRule(RelativeLayout.ALIGN_START, R.id.etQuote);
        etAuthor.setLayoutParams(authorParams);

        Log.d(TAG,"render widget done. left:"+left+" top:"+top);
    }

    public String toString() {
        return mHashMap.toString();
    }

    public static int getFontSize(int quoteLen) {
        if (quoteLen>130) return (32);
        else if (quoteLen>150) return (28);
        else if (quoteLen>180) return (24);
        else if (quoteLen>200) return (18);

        return 40;  //default
    }
}
