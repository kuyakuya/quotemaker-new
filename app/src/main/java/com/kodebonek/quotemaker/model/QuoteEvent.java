package com.kodebonek.quotemaker.model;

import android.net.Uri;

public class QuoteEvent {

    public static int SAVED_SUCCESS = 1;
    public static int SAVED_FAILED  = 2;
    public static int IMAGE_EXIST = 3;

    public int type;
    public String failReason = "";
    public Uri savedImageUri;
    public String quoteID;

    private QuoteEvent(int type) {
        this.type = type;
    }

    public static QuoteEvent saveSuccess(Uri uri,String quoteID) {
        QuoteEvent event = new QuoteEvent(SAVED_SUCCESS);
        event.savedImageUri = uri;
        event.quoteID = quoteID;
        return event;
    }

    public static QuoteEvent saveFailed(String reason) {
        QuoteEvent event = new QuoteEvent(SAVED_FAILED);
        event.failReason = reason;
        return event;
    }
    public static QuoteEvent imageExist(Uri uri,String quoteID) {
        QuoteEvent event = new QuoteEvent(IMAGE_EXIST);
        event.savedImageUri = uri;
        event.quoteID = quoteID;
        return event;
    }
}
