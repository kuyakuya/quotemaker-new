package com.kodebonek.quotemaker;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kodebonek.quotemaker.model.Quote;
import com.kodebonek.quotemaker.model.QuoteEvent;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.utils.MySurfaceRenderer;
import com.kodebonek.quotemaker.widget.NonSwipeableViewPager;
import com.kodebonek.quotemaker.widget.QuoteEditText;

import de.greenrobot.event.EventBus;

public class EditorActivity extends AppCompatActivity {

    private static final String TAG = "EditorActivity";

    private Quote mQuote = null;
    private MySurfaceRenderer mRenderer;

    private QuoteEditText etQuote = null;
    private QuoteEditText etAuthor = null;
    private FrameLayout frameContainer = null;
    private RelativeLayout frameDrag = null;
    private int mDeviceWidth;

    //Declare this flag globally
    private int _xDelta;
    private int _yDelta;
    private int _selStart,_selEnd;

    private GLSurfaceView theBackground = null;

    private boolean mEdited = false;
    private boolean mHasSaved = false;
    private boolean mOnCreateEvent = true;
    private boolean mFinishAfterSave = false;
    private AlertDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String quoteID = "";
        String quoteAuthor = "";    //for new only
        String quoteMessage = "";   //for new only
        if (savedInstanceState!=null) {
            quoteID = savedInstanceState.getString("quoteID");
        } else {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                quoteID = extras.getString("quoteID");
                quoteAuthor = extras.getString("quoteAuthor");
                quoteMessage = extras.getString("quoteMessage");
            }
        }
        setContentView(R.layout.activity_editor);

        mOnCreateEvent = true;
        setupToolbar();
        setupTabs();
        setupTouchListener();
        setupLayout();
        setupSurfaceView();

        if (quoteID!=null && quoteID.length()>0) {
            mQuote = Quote.newQuote(this, quoteID);
            changeBackground(mQuote.getBackground());
            mHasSaved = true;
            Log.d(TAG,"quote data => "+mQuote.toString());
        } else {
            mQuote = Quote.newQuote(this);
            mQuote.setAuthor(quoteAuthor);
            mQuote.setQuote(quoteMessage);
            mQuote.setTextSize(Quote.getFontSize(quoteMessage.length()));
            mHasSaved = false;
        }
        mQuote.setQuoteWidget(etQuote);
        mQuote.setAuthorWidget(etAuthor);
        mQuote.renderWidget();

        Log.d(TAG, "frameDrag left:" + frameDrag.getLeft() + " top:" + frameDrag.getTop());
        Log.d(TAG,"etQuote left:"+etQuote.getLeft()+" top:"+etQuote.getTop());
        mOnCreateEvent = false;
        mEdited = false;

        //lock on portrait mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    protected void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState()");
        outState.putString("quoteID", mQuote.getQuoteID());
        super.onSaveInstanceState(outState);
    }

    private void setupSurfaceView() {
        /**
         * Initialize the renderer and tell it to only render when
         * explicity requested with the RENDERMODE_WHEN_DIRTY option
         */
        theBackground = (GLSurfaceView) findViewById(R.id.surface_background);
        theBackground.setEGLContextClientVersion(2);
        //theBackground.setRenderer(this);
        mRenderer = new MySurfaceRenderer(this);
        mRenderer.setGLSurfaceView(theBackground);
        theBackground.setRenderer(mRenderer);
        theBackground.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        theBackground.setDebugFlags(GLSurfaceView.DEBUG_CHECK_GL_ERROR);
    }

    private void setupTouchListener() {
        //setup drag listener for BG and text
        frameDrag = (RelativeLayout) findViewById(R.id.frame_to_drag);
        frameDrag.setOnTouchListener(new FrameLayoutTouchListener());

        etQuote = (QuoteEditText) findViewById(R.id.etQuote);
        etQuote.setOnTouchListener(new QuoteTouchListener());
        etQuote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mOnCreateEvent) mEdited = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //etQuote.setOnTouchListener(new FrameLayoutTouchListener());
        etAuthor = (QuoteEditText) findViewById(R.id.etAuthor);
        etAuthor.setOnTouchListener(new QuoteTouchListener());
        etAuthor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mOnCreateEvent) mEdited = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //set hide cursor logic
        etQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etQuote.setCursorVisible(true);
            }
        });
        etAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAuthor.setCursorVisible(true);
            }
        });
    }

    private void setupLayout() {
        //setup square format
        mDeviceWidth = Helper.getDeviceWidth(getApplicationContext());
        frameContainer = (FrameLayout) findViewById(R.id.frame_container);
        //frameContainer.setMinimumHeight(mDeviceWidth);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mDeviceWidth, mDeviceWidth);    //square format
        frameContainer.setLayoutParams(lp);

        /*
        create layout like this -->

        +---+-------+---+
        | - | -     | - |
        +---+-------+---+
        | - | Quote | - |
        +---+-------+---+
        | - | -     | - |
        +---+-------+---+
        box_width = deviceWidth (1080px)
         */

        FrameLayout.LayoutParams params  = new FrameLayout.LayoutParams(mDeviceWidth*3,mDeviceWidth * 3);
        params.setMargins(-1 * mDeviceWidth, -1 * mDeviceWidth, 0, 0);
        frameDrag.setLayoutParams(params);
    }

    private void setupTabs() {
        EditorPagerAdapter editorPagerAdapter = new EditorPagerAdapter(getSupportFragmentManager());
        final NonSwipeableViewPager viewPager = (NonSwipeableViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(editorPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) return;

        setSupportActionBar(toolbar);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEdited) {
                    mFinishAfterSave = true;
                    askSaveBeforeExit();
                } else {
                    if (mHasSaved) activityFinishSuccess();
                    else activityFinishCancel();
                }
            }
        });

        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProject();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mEdited) {
            mFinishAfterSave = true;
            askSaveBeforeExit();
        } else {
            //exit activity
            if (mHasSaved) activityFinishSuccess();
            else activityFinishCancel();
        }
    }

    private void activityFinishSuccess() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("quoteID",mQuote.getQuoteID());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void activityFinishCancel() {
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
/*
        if (id == R.id.action_new_quote) {
            if (mEdited) {
                askSaveBeforeExit();
            }
            //TODO initiate new qoute
            return true;
        }
*/

        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(QuoteEvent event) {
        int event_type = event.type;
        if (event_type == QuoteEvent.SAVED_SUCCESS) {
            Log.d(TAG, "received event: SAVED_SUCCESS");
            showProgress(false);
            toastUI("Quote saved!");
            mEdited = false;
            mHasSaved = true;

            if (mFinishAfterSave) activityFinishSuccess();

        } else if (event_type == QuoteEvent.SAVED_FAILED) {
            Log.d(TAG, "received event: SAVED_FAILED");
            showProgress(false);
            toastUI(event.failReason);
            mFinishAfterSave = false;
        } else {
            Log.d(TAG,"received unknown event: "+event_type);
        }

    }

    private void askSaveBeforeExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save")
                .setMessage("Save your changes?")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveProject();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (mHasSaved) activityFinishSuccess();
                        else activityFinishCancel();
                    }
                })
                .show();
    }

    private void saveProject() {

        //clear any focus,select,cursor etc on text
        etQuote.clearComposingText();
        etQuote.clearComposingText();

        showProgress(true);
        mRenderer.runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "saveQuote begin...");

                Bitmap bgBitmap = mRenderer.getBitmapFromGLSurface();   //get background only
                if (bgBitmap == null) {
                    EventBus.getDefault().post(QuoteEvent.saveFailed("failed processing bitmap"));
                    return;
                }
                Bitmap quoteBitmap = saveQuoteToImage();
                if (quoteBitmap == null) {
                    EventBus.getDefault().post(QuoteEvent.saveFailed("failed saving bitmap"));
                    return;
                }

                if (mQuote != null) {
                    int left = frameDrag.getLeft() + etQuote.getLeft();
                    int top = frameDrag.getTop() + etQuote.getTop();

                    boolean resultOK = mQuote.saveToFile(left, top, quoteBitmap, bgBitmap);
                    quoteBitmap.recycle();
                    bgBitmap.recycle();
                    System.gc();

                    if (resultOK)
                        EventBus.getDefault().post(QuoteEvent.saveSuccess(mQuote.getThumbnail(),mQuote.getQuoteID()));
                    else
                        EventBus.getDefault().post(QuoteEvent.saveFailed("save failed!"));

                } else {
                    Log.e(TAG, "unexpected null mQuote");
                    EventBus.getDefault().post(QuoteEvent.saveFailed("unexpected failure!"));
                    return;
                }

                Log.d(TAG, "saveQuote finish");
            }
        });

        theBackground.requestRender();
    }

    public Uri beginSharing() {
        if (mHasSaved==true && mEdited==false) {
            //get already saved image and share it
            Uri quoteImage = mQuote.getThumbnail();
            EventBus.getDefault().post(QuoteEvent.imageExist(quoteImage,mQuote.getQuoteID()));
            return quoteImage;
        } else {
            //new project or just edited
            saveProject();
            return null;
        }
    }

    private Bitmap saveQuoteToImage() {

        Log.d(TAG, "saveQuoteToImage begin...");
        Bitmap bg = mRenderer.getBitmapFromGLSurface();
        if (bg!=null) {
            //convert to mutable
            Bitmap.Config bitmapConfig = bg.getConfig();
            if(bitmapConfig == null) {
                bitmapConfig = Bitmap.Config.ARGB_8888;
            }
            bg = bg.copy(bitmapConfig, true);

            //draw edittext to bitmap
            Canvas canvas = new Canvas(bg);
            etQuote.buildDrawingCache();
            Bitmap cacheBitmap = etQuote.getDrawingCache();
            if (cacheBitmap!=null) {
                int left = frameDrag.getLeft() + etQuote.getLeft();
                int top = frameDrag.getTop() + etQuote.getTop();
                canvas.drawBitmap(cacheBitmap, left, top, null);
                etQuote.destroyDrawingCache();

                etAuthor.buildDrawingCache();
                cacheBitmap = etAuthor.getDrawingCache();
                if (cacheBitmap!=null) {
                    left = frameDrag.getLeft() + etAuthor.getLeft();
                    top = frameDrag.getTop() + etAuthor.getTop();
                    canvas.drawBitmap(cacheBitmap, left, top, null);
                    etAuthor.destroyDrawingCache();
                }
            } else {
                Log.w(TAG,"etQuote getDrawingCache return null");
            }

            Log.d(TAG,"saveQuoteToImage finish.");
            return(bg);

        } else {
            Log.e(TAG, "saveQuoteToImage failed. getBitmapFromGLSurface return null!");
            return null;
        }
    }

    private void toastUI(final String message) {
        Toast.makeText(EditorActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    private void showProgress(final boolean show) {
        if (show) {
            //pb.bringToFront();
            //pb.setVisibility(View.VISIBLE);
            if (progressDialog!=null && progressDialog.isShowing())
                progressDialog.dismiss();

            setupProgressDialog();
            progressDialog.show();
        } else {
            //pb.setVisibility(View.INVISIBLE);
            if (progressDialog==null) return;

            progressDialog.dismiss();
        }
    }

    private void setupProgressDialog() {
        progressDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.progress_dialog, null);
        progressDialog.setView(layout);
        ((TextView)layout.findViewById(R.id.textView)).setText("Saving ... please wait");
    }

    public void changeBackground(Uri bitmapUri) {
        Log.d(TAG, "changeBackground: " + bitmapUri.getPath());
        if (!mOnCreateEvent) mEdited = true;

        BitmapFactory.Options options = new BitmapFactory.Options();
        //checking bitmap size first..
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(bitmapUri.getPath(), options);
        Log.d(TAG, "bitmap dimension. w:" + options.outWidth + " h:" + options.outHeight);

        if (options.outWidth<mDeviceWidth || options.outHeight<mDeviceWidth) {
            //resize up that shit
            Log.d(TAG,"enlarge bitmap to fill screen: "+mDeviceWidth);
            options.inJustDecodeBounds = false;
            Bitmap orgBitmap = BitmapFactory.decodeFile(bitmapUri.getPath(), options);
            Bitmap resizeBitmap = Bitmap.createScaledBitmap(orgBitmap,mDeviceWidth,mDeviceWidth,true);

            orgBitmap.recycle();
            mRenderer.changeSurfaceBitmap(resizeBitmap);
        } else {
            //simple scaling is enough
            int scaleFactor = Math.min(options.outWidth / mDeviceWidth, options.outHeight / mDeviceWidth);
            Log.d(TAG,"decode bitmap with scalefactor of "+scaleFactor);

            options.inJustDecodeBounds = false;
            options.inSampleSize = scaleFactor;
            Bitmap bitmap = BitmapFactory.decodeFile(bitmapUri.getPath(), options);
            mRenderer.changeSurfaceBitmap(bitmap);
        }

    }

    public void changeBackground(int color) {
        Log.d(TAG, "changeBackground with solid color: " + color);
        mEdited = true;
        Bitmap bitmap = Bitmap.createBitmap(mDeviceWidth, mDeviceWidth, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(color);
        mRenderer.changeSurfaceBitmap(bitmap);
    }

    public void changeBackground(int color1,int color2) {
        Log.d(TAG, "changeBackground with gradient color: " + color1 + " -> " + color2);
        mEdited = true;
        Bitmap bitmap = Helper.createGradientBitmap(this,color1, color2);
        mRenderer.changeSurfaceBitmap(bitmap);
    }

    public void changeTextFont(String font) { if (mQuote!=null) mQuote.setFont(font); mEdited = true; }
    public void changeTextColor(int color) { if (mQuote!=null) mQuote.setColor(color); mEdited = true; }
    public void changeTextSize(int size) { if (mQuote!=null) mQuote.setTextSize(size); mEdited = true; }
    public void changeTextAlignment(int gravity) { if (mQuote!=null) mQuote.setAlignment(gravity); mEdited = true; }
    public void changeTextLineHeight(float val) { if (mQuote!=null) mQuote.setLineHeight(val); mEdited = true; }
    public int getTextSize() { return (int) Helper.pixelsToSp(this,etQuote.getTextSize()); }

    //accessed by control
    public String getAuthor() { return etAuthor.getText().toString(); }
    public void changeAuthor(String newauthor) {
        etAuthor.setText(newauthor);
        mEdited = true;
    }

    public boolean fxOpen(String effectname) { return mRenderer.createEffect(effectname);  }

    public void fxApply() {
        Log.d(TAG, "effect applied");
        mRenderer.swapTexture();
        mEdited = true;
    }

    public void fxUpdateBrightness(float level) { mRenderer.setBrightness(level); }
    public void fxUpdateContrast(float level) {  mRenderer.setContrast(level); }
    public void fxUpdateAutoFix(float scale) {  mRenderer.setAutoFix(scale); }
    public void fxUpdateSaturate(float scale) {  mRenderer.setSaturation(scale); }
    public void fxUpdateTemperature(float scale) { mRenderer.setTemperature(scale); }
    public void fxUpdateTint(int color) {
        int argb = Color.argb(0xff,Color.red(color),Color.green(color),Color.blue(color));
        mRenderer.setTintColor(argb);
    }
    public void fxUpdateVignette(float scale) { mRenderer.setVignette(scale);  }
    public void fxUpdateDuotone(int selectedColor1, int selectedColor2) {
        int argb1 = Color.argb(0xff,Color.red(selectedColor1),Color.green(selectedColor1),Color.blue(selectedColor1));
        int argb2 = Color.argb(0xff,Color.red(selectedColor2),Color.green(selectedColor2),Color.blue(selectedColor2));
        mRenderer.setDuotoneColor(argb1,argb2);
    }
    public void fxUpdateFilllight(float level) { mRenderer.setFillLight(level); }
    public void fxUpdateGrain(float level) { mRenderer.setGrain(level); }
    public void fxUpdateFilter(String effectname) { mRenderer.setFilter(effectname); }

    private class EditorPagerAdapter extends FragmentPagerAdapter {

        private final int TABS_COUNT = 4;
        private String tabTitles[] = new String[] { "Background", "Text", "Effects" , "Share"};

        public EditorPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return BackgroundFragment.newInstance(position);
                case 1:
                    return TextFragment.newInstance(position);
                case 2:
                    return EffectFragment.newInstance(position);
                case 3:
                    return ShareFragment.newInstance(position);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return TABS_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }

    private final class FrameLayoutTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                    //RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) frameDrag.getLayoutParams();
                    _xDelta = X - lParams.leftMargin;
                    _yDelta = Y - lParams.topMargin;
                    mEdited = true;
                    break;
                case MotionEvent.ACTION_MOVE:
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                    //RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameDrag.getLayoutParams();
                    layoutParams.leftMargin = X - _xDelta;
                    layoutParams.topMargin = Y - _yDelta;
                    view.setLayoutParams(layoutParams);
                    mEdited = true;
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    private final class QuoteTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();

            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) frameDrag.getLayoutParams();
                //RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) frameDrag.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;

                _selStart = etQuote.getSelectionStart();
                _selEnd = etQuote.getSelectionEnd();
                mEdited = true;

            } else if (event.getAction() == MotionEvent.ACTION_UP) {


            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameDrag.getLayoutParams();
                //RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameDrag.getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                frameDrag.setLayoutParams(layoutParams);
                mEdited = true;
            }

            return false;   //pass it to other
        }
    }

}
