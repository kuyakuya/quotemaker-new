package com.kodebonek.quotemaker;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.kodebonek.quotemaker.model.Quote;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class YourQuoteFragment extends Fragment {

    public static final String ARG_POSITION = "ARG_POSITION";
    private static final String TAG = "YourQuoteFragment";
    private static final int NEW_CONTENT_ID = 0;
    public static final int REQUEST_CODE_NEW_QUOTE = 5000;
    public static final int REQUEST_CODE_EDIT_QUOTE = 5001;
    private int mPosition;
    private FloatingActionButton fabAdd = null;
    private SavedQuoteAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;
    private boolean canRemoveData;
    private RecyclerView mRecyclerView;
    private RelativeLayout mEmptyView;

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);

        YourQuoteFragment fragment = new YourQuoteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //get context via getActivity()
        View view = inflater.inflate(R.layout.fragment_your_quote, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Log.d(TAG,"onViewCreated()");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mEmptyView = (RelativeLayout) view.findViewById(R.id.empty_view);

        fabAdd = (FloatingActionButton) view.findViewById(R.id.fab_add_content);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), EditorActivity.class);
                //myIntent.putExtra("quoteID", "");
                startActivityForResult(myIntent, REQUEST_CODE_NEW_QUOTE);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_layout);

        final GridLayoutManager gridManager = new GridLayoutManager(getActivity(),2);
        mRecyclerView.setLayoutManager(gridManager);
        mAdapter = new SavedQuoteAdapter();
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                checkIsAdapterEmpty();
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        checkIsAdapterEmpty();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.reloadQuotes();
                mAdapter.notifyDataSetChanged();
                mRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        //logic for swiperefreshlayout on scroll
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView mRecyclerView, int dx, int dy) {
                //super.onScrolled(mRecyclerView, dx, dy);
                int pos = gridManager.findFirstCompletelyVisibleItemPosition();
                if (pos==0)
                    mSwipeRefreshLayout.setEnabled(true);
                else
                    mSwipeRefreshLayout.setEnabled(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void checkIsAdapterEmpty() {
        if (mAdapter.getItemCount()==0) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setEnabled(false);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
            mSwipeRefreshLayout.setEnabled(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQUEST_CODE_NEW_QUOTE && resultCode== Activity.RESULT_OK) {
            String quoteID = data.getStringExtra("quoteID");
            if (quoteID.length()>0) {
                Quote quote = Quote.newQuote(getActivity(),quoteID);
                mAdapter.add(quote);
            }
        } else if (requestCode==REQUEST_CODE_EDIT_QUOTE && resultCode== Activity.RESULT_OK) {
            String quoteID = data.getStringExtra("quoteID");
            if (quoteID.length()>0) {
                mAdapter.update(quoteID);
            }
        }
    }

    public void reloadQuotes() {
        if (mAdapter==null) return;

        mAdapter.reloadQuotes();
        mAdapter.notifyDataSetChanged();
    }

    private class SavedQuoteAdapter extends RecyclerView.Adapter {

        private ArrayList<Quote> quotes;

        public SavedQuoteAdapter() {
            quotes = new ArrayList<Quote>();
            reloadQuotes();
        }

        public void reloadQuotes() {
            ArrayList<Quote> list = Quote.getQuotes(getActivity(), 0, 10);
            if (list==null) return;

            quotes.clear();
            quotes.addAll(list);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_save, viewGroup, false);

            QuoteViewHolder viewHolder = new QuoteViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            QuoteViewHolder holder = (QuoteViewHolder) viewHolder;
            Quote quote = quotes.get(i);
            if (quote==null) {
                Log.w(TAG,"quote is null !!! already deleted??");
                return;
            }

            //holder.textView.setText(quote.getQuoteID());
            ClickListener clickListener = new ClickListener();

            holder.btnEdit.setTag(quote.getQuoteID());
            holder.btnEdit.setOnClickListener(clickListener);
            holder.btnDelete.setTag(quote.getQuoteID());
            holder.btnDelete.setOnClickListener(clickListener);
            holder.btnShare.setTag(quote.getQuoteID());
            holder.btnShare.setOnClickListener(clickListener);


            Uri thumbnail = quote.getThumbnail();
            if (thumbnail==null) {
                Log.e(TAG,"oops, quote thumbnail already deleted :( !!!");
                return;
            }

            Log.d(TAG, "picasso loading " + thumbnail.getPath());
            holder.imageView.setImageBitmap(null);
            Picasso.with(getActivity()).cancelRequest(holder.imageView);
            //Picasso.with(getActivity()).invalidate(thumbnail);
            Picasso.with(getActivity())
                    .load(thumbnail)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .resize(300, 300)
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "picasso load success");
                        }

                        @Override
                        public void onError() {
                            Log.d(TAG, "picasso load failed");
                        }
                    });

        }

        @Override
        public int getItemCount() {
            return quotes.size();
        }

        private Quote getQuote(String quoteID) {
            boolean found = false;
            Quote quote = null;
            for (int i=0; i < quotes.size(); i++) {
                quote = quotes.get(i);
                if (quote!=null && quote.getQuoteID().equalsIgnoreCase(quoteID)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                Log.e(TAG,"quote "+quoteID+" not found");
                return null;
            }
            else return quote;
        }

        public boolean add(Quote quote) {
            if (quote==null) return false;

            int position = quotes.size();
            quotes.add(position, quote);
            notifyItemInserted(position);
            return true;
        }

        public boolean remove(Quote quote) {
            int position = quotes.indexOf(quote);
            if (position<0) {
                Log.e(TAG,"unable to remove quote: "+quote.getQuoteID());
                return false;
            }
            //quote.deleteQuoteFiles();
            quotes.remove(position);
            notifyItemRemoved(position);
            return true;
        }

        public boolean remove(String quoteID) {
            Quote quote = getQuote(quoteID);
            if (quote==null) return false;
            return remove(quote);
         }

        public boolean update(Quote quote) {
            int position = quotes.indexOf(quote);
            if (position<0) {
                Log.e(TAG,"unable to get quote: "+quote.getQuoteID());
                return false;
            }
            notifyItemChanged(position);
            return true;
        }

        public boolean update(String quoteID) {
            Quote quote = getQuote(quoteID);
            if (quote==null) return false;
            return update(quote);
        }

        private class ClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                final String quoteID = (String) v.getTag();

                if (v.getId()==R.id.buttonEdit) {
                    //Toast.makeText(getActivity(),"Edit "+quoteID,Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(getActivity(), EditorActivity.class);
                    myIntent.putExtra("quoteID", quoteID);
                    startActivityForResult(myIntent, REQUEST_CODE_EDIT_QUOTE);

                } else if (v.getId()==R.id.buttonDelete) {
                    Log.d(TAG,"deleting quote ");
                    canRemoveData = true;
                    final Quote quote = getQuote(quoteID);
                    remove(quote);  //ui remove

                    //Toast.makeText(getActivity(),"Delete "+quoteID,Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar.make(getView(), "Quote deleted", Snackbar.LENGTH_SHORT);
                    snackbar.setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG,"undo deleting quote");
                            canRemoveData = false;
                            //re add to queue
                            add(quote);
                        }
                    });
                    snackbar.show();

                    new Handler(getActivity().getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (canRemoveData) {
                                quote.deleteQuoteFiles();
                            }
                        }
                    }, (int) (2000 * 1.0f));    //TODO replace with better code, this is Google snackbar current bug (getDuration() return 0)

                } else if (v.getId()==R.id.buttonShare) {

                    //get image
                    Quote quote = getQuote(quoteID);
                    Uri uri = FileProvider.getUriForFile(getActivity(), "com.kodebonek.quotemaker.EditorActivity", new File(quote.getThumbnail().getPath()));

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    //grant permisions for all apps that can handle given intent
                    List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }

                    startActivity(Intent.createChooser(shareIntent, "Share to other app"));
                }
            }
        }
    }

    private class QuoteViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        //protected TextView textView;
        protected ImageButton btnEdit;
        protected ImageButton btnDelete;
        protected ImageButton btnShare;

        public QuoteViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            //this.textView = (TextView) view.findViewById(R.id.textView);
            this.btnEdit = (ImageButton) view.findViewById(R.id.buttonEdit);
            this.btnDelete = (ImageButton) view.findViewById(R.id.buttonDelete);
            this.btnShare = (ImageButton) view.findViewById(R.id.buttonShare);
        }
    }
}
