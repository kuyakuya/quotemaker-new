package com.kodebonek.quotemaker;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.kodebonek.quotemaker.controls.ControlTextAlignFragment;
import com.kodebonek.quotemaker.controls.ControlTextAuthorFragment;
import com.kodebonek.quotemaker.controls.ControlTextColorFragment;
import com.kodebonek.quotemaker.controls.ControlTextFontFragment;
import com.kodebonek.quotemaker.controls.ControlTextLetterSpacingFragment;
import com.kodebonek.quotemaker.controls.ControlTextLineHeightFragment;
import com.kodebonek.quotemaker.controls.ControlTextSizeFragment;
import com.kodebonek.quotemaker.utils.Helper;

import java.util.List;

public class TextFragment extends Fragment {
    public static final String ARG_POSITION = "ARG_POSITION";
    private int mPosition;
    private static final String TAG = "TextFragment";

    private ControlTextFontFragment mControlTextFontFragment;
    private ControlTextColorFragment mControlTextColorFragment;
    private ControlTextSizeFragment mControlTextSizeFragment;
    private ControlTextAlignFragment mControlTextAlignFragment;
    private ControlTextLineHeightFragment mControlTextLineHeightFragment;
    private ControlTextLetterSpacingFragment mControlTextLetterSpacingFragment;
    private ControlTextAuthorFragment mControlTextAuthorFragment;

    Button lastClickedButton = null;
    FrameLayout flControlContainer = null;
    private PleaseSelectFragment mPleaseSelectFragment;

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);

        TextFragment fragment = new TextFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //get context via getActivity()
        View view = inflater.inflate(R.layout.fragment_text, container, false);

        Button btnTextFont = (Button) view.findViewById(R.id.btnTextFont);
        Helper.styleButton(btnTextFont);
        Button btnTextColor = (Button) view.findViewById(R.id.btnTextColor);
        Helper.styleButton(btnTextColor);
        Button btnTextSize = (Button) view.findViewById(R.id.btnTextSize);
        Helper.styleButton(btnTextSize);
        Button btnTextAlignment = (Button) view.findViewById(R.id.btnTextAlignment);
        Helper.styleButton(btnTextAlignment);
        Button btnTextLineHeight = (Button) view.findViewById(R.id.btnTextLineHeight);
        Helper.styleButton(btnTextLineHeight);
        Button btnAuthor = (Button) view.findViewById(R.id.btnTextAuthor);
        Helper.styleButton(btnAuthor);
        //Button btnTextLetterSpacing = (Button) view.findViewById(R.id.btnTextLetterSpacing);

        flControlContainer = (FrameLayout) view.findViewById(R.id.control_fragment);

        btnTextFont.setOnClickListener(new StartSelectFont());
        btnTextColor.setOnClickListener(new StartSelectColor());
        btnTextSize.setOnClickListener(new StartSelectSize());
        btnTextAlignment.setOnClickListener(new StartSelectAlignment());
        btnTextLineHeight.setOnClickListener(new StartSelectLineHeight());
        btnAuthor.setOnClickListener(new StartAuthor());
        //btnTextLetterSpacing.setOnClickListener(new StartSelectLetterSpacing());

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clearSelectedViews();
    }

    private class StartSelectFont implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextFontFragment ==null) {
                mControlTextFontFragment = new ControlTextFontFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextFontFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private class StartSelectColor implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextColorFragment ==null) {
                mControlTextColorFragment = new ControlTextColorFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextColorFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private class StartSelectSize implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextSizeFragment ==null) {
                mControlTextSizeFragment = new ControlTextSizeFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextSizeFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private class StartSelectAlignment implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextAlignFragment ==null) {
                mControlTextAlignFragment = new ControlTextAlignFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextAlignFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private class StartSelectLineHeight implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextLineHeightFragment ==null) {
                mControlTextLineHeightFragment = new ControlTextLineHeightFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextLineHeightFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private class StartSelectLetterSpacing implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextLetterSpacingFragment ==null) {
                mControlTextLetterSpacingFragment = new ControlTextLetterSpacingFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextLetterSpacingFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }


    private class StartAuthor implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlTextAuthorFragment ==null) {
                mControlTextAuthorFragment = new ControlTextAuthorFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlTextAuthorFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private void highlightSelectedViews(View v) {

        if (lastClickedButton!=null) {
            //restore to normal
            lastClickedButton.setBackgroundColor(Color.TRANSPARENT);
        }

        Button btn = (Button)v;
        btn.setBackgroundColor(Color.parseColor("#DDDDDD"));
        flControlContainer.setBackgroundColor(Color.parseColor("#DDDDDD"));
        lastClickedButton = btn;
    }

    private void clearSelectedViews() {
        List<Fragment> al = getChildFragmentManager().getFragments();
        if (al != null) {
            for (Fragment frag : al) {
                getChildFragmentManager().beginTransaction().remove(frag).commit();
            }
        }

        flControlContainer.setBackgroundColor(Color.TRANSPARENT);
        if (lastClickedButton!=null) {
            lastClickedButton.setBackgroundColor(Color.TRANSPARENT);
            lastClickedButton = null;
        }

        if (mPleaseSelectFragment==null) {
            mPleaseSelectFragment = new PleaseSelectFragment();
        }

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.control_fragment, mPleaseSelectFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

}
