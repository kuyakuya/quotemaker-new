package com.kodebonek.quotemaker.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class QuoteEditText extends EditText {

    public static String SEPARATOR = "\r\n";

    public QuoteEditText(Context context) {
        super(context);
    }

    public QuoteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuoteEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK &&  event.getAction() == KeyEvent.ACTION_UP) {
            setSelection(getText().length());
            setCursorVisible(false);
            clearFocus();
            return false;
        }
        return super.onKeyPreIme(keyCode, event);
    }

}
