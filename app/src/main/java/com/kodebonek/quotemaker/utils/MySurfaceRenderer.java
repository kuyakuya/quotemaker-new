package com.kodebonek.quotemaker.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;

import com.kodebonek.quotemaker.R;
import com.kodebonek.quotemaker.utils.GLToolbox;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.utils.TextureRenderer;

import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MySurfaceRenderer implements GLSurfaceView.Renderer {
    private static final String TAG = "MyRenderer";
    private Context mContext;
    private EffectContext mEffectContext;
    private Effect mEffect;

    private Uri mBgFileToLoad = null;
    private int mDeviceWidth = 0;
    private TextureRenderer mTexRenderer = null;
    private int[] mTextures = new int[2];
    private boolean mSurfaceInitialized = false;
    private int mImageWidth,mImageHeight;
    private boolean mEffectInProgress = false;

    //private Bitmap tempBitmap = null;
    private GLSurfaceView mSurfaceView = null;
    private final Queue<Runnable> mRunOnDrawBegin;
    private final Queue<Runnable> mRunOnDrawEnd;

    public MySurfaceRenderer(Context ctx) {
        this.mContext = ctx;
        this.mDeviceWidth = Helper.getDeviceWidth(ctx);
        mTexRenderer = new TextureRenderer();
        mRunOnDrawBegin = new LinkedList<Runnable>();
        mRunOnDrawEnd = new LinkedList<Runnable>();
    }

    public void setGLSurfaceView(GLSurfaceView surfaceView) {
        this.mSurfaceView = surfaceView;
    }

    private void initTextures() {
        // Generate textures
        GLES20.glGenTextures(mTextures.length, mTextures, 0);

        Bitmap bitmap;
        if (mBgFileToLoad==null) {
            //no file as background .. use random gradient instead
            List<String> colors = Arrays.asList(mContext.getResources().getStringArray(R.array.color_palette));
            Collections.shuffle(colors);
            int color1 = Color.parseColor(colors.get(0));
            Collections.shuffle(colors);
            int color2 = Color.parseColor(colors.get(0));

            bitmap = Helper.createGradientBitmap(mContext,color1, color2);
        } else {
            bitmap = BitmapFactory.decodeFile(mBgFileToLoad.getPath());
        }

        mImageWidth = bitmap.getWidth();
        mImageHeight = bitmap.getHeight();
        mTexRenderer.updateTextureSize(mImageWidth, mImageHeight);

        // Upload to texture
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

        // Set texture parameters
        GLToolbox.initTexParams();
        bitmap.recycle();
    }


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d(TAG, "onSurfaceCreated()");
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(TAG, "onSurfaceChanged()");
/*
        if (tempBitmap!=null) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
            GLUtils.texSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0, tempBitmap);
            tempBitmap.recycle();
            tempBitmap = null;
        }
*/
        if (mTexRenderer != null) {
            mTexRenderer.updateViewSize(width, height);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        Log.d(TAG, "onDrawFrame()");
        if (!mSurfaceInitialized) {
            //Only need to do this once
            mEffectContext = EffectContext.createWithCurrentGlContext();
            mTexRenderer.init();
            initTextures();
            mSurfaceInitialized = true;
        }

        //run shit on begin draw -> set bitmap background
        Log.d(TAG,"runOnDrawBegin..");
        runAll(mRunOnDrawBegin);


        if (mEffectInProgress && mEffect!=null) {
            mEffect.apply(mTextures[0], mImageWidth, mImageHeight, mTextures[1]);
        }

        //render it and then we can save the image
        renderResult();

        //do our shits here..
        //https://github.com/CyberAgent/android-gpuimage/blob/master/library/src/jp/co/cyberagent/android/gpuimage/GPUImageRenderer.java
        Log.d(TAG,"runOnDrawEnd..");
        runAll(mRunOnDrawEnd);
    }

    private void renderResult() {
        if (mEffectInProgress) {
            mTexRenderer.renderTexture(mTextures[1]);
            mEffectInProgress = false;
        } else {
            mTexRenderer.renderTexture(mTextures[0]);
        }
    }

    private void runAll(Queue<Runnable> queue) {
        synchronized (queue) {
            while (!queue.isEmpty()) {
                queue.poll().run();
            }
        }
    }

    public void runOnDraw(final Runnable runnable) {
        synchronized (mRunOnDrawBegin) {
            mRunOnDrawBegin.add(runnable);
        }
    }

    public void runOnGLThread(final Runnable runnable) {
        synchronized (mRunOnDrawEnd) {
            mRunOnDrawEnd.add(runnable);
        }
    }

    public boolean changeSurfaceBitmap(final Bitmap bitmap) {
        if (bitmap==null) return false;

        //tempBitmap = bitmap;
        runOnDraw(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "changeSurfaceBitmap begin...");
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
                GLUtils.texSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0, bitmap);
                if (bitmap!=null) bitmap.recycle();
                Log.d(TAG, "changeSurfaceBitmap finish.");
            }
        });
        mSurfaceView.requestRender();
        return true;
    }

    public Bitmap getBitmapFromGLSurface()
            throws OutOfMemoryError {
        int w = mDeviceWidth;
        int h = mDeviceWidth;

        Log.d(TAG,"getBitmapFromGLSurface begin...");

        int[] pixelMirroredArray = new int[w * h];
        IntBuffer pixelBuffer = IntBuffer.allocate(w * h);
        GLES20.glReadPixels(0, 0, w, h, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, pixelBuffer);
        int[] pixelArray = pixelBuffer.array();

        // Convert upside down mirror-reversed image to right-side up normal image.
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                pixelMirroredArray[(h - i - 1) * w + j] = pixelArray[i * w + j];
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(IntBuffer.wrap(pixelMirroredArray));
        pixelMirroredArray = null;
        pixelBuffer.clear();

        Log.d(TAG, "getBitmapFromGLSurface finish.");
        return bitmap;
    }

    public boolean createEffect(String effectname) {
        try {
            //release previous effect
            if (mEffect!=null) mEffect.release();

            if (mEffectContext.getFactory().isEffectSupported(effectname)) {
                mEffect = mEffectContext.getFactory().createEffect(effectname);
                if (mEffect!=null)
                    return true;
                else {
                    Log.e(TAG, "failed creating effect: "+effectname);
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG,"exception createEffect(): "+e.toString());
            return false;
        }
    }

    public void setBrightness(float level) {
        mEffect.setParameter("brightness", level);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setContrast(float level) {
        mEffect.setParameter("contrast", level);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setAutoFix(float scale) {
        mEffect.setParameter("scale", scale);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setSaturation(float scale) {
        mEffect.setParameter("scale", scale);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setTemperature(float scale) {
        mEffect.setParameter("scale", scale);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setTintColor(int argb) {
        mEffect.setParameter("tint", argb);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setVignette(float scale) {
        mEffect.setParameter("scale", scale);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setDuotoneColor(int argb1, int argb2) {
        mEffect.setParameter("first_color", argb1);
        mEffect.setParameter("second_color", argb2);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setFillLight(float level) {
        mEffect.setParameter("strength", level);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setGrain(float level) {
        mEffect.setParameter("strength", level);
        mEffectInProgress = true;
        mSurfaceView.requestRender();
    }

    public void setFilter(String effectname) {
        if (createEffect(effectname)) {
            mEffectInProgress = true;
            mSurfaceView.requestRender();
        }
    }

    public void swapTexture() {
        //a.k.a applying the filter
        int temp = mTextures[0];
        mTextures[0] = mTextures[1];
        mTextures[1] = temp;
    }
}
