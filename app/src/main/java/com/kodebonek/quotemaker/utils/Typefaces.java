package com.kodebonek.quotemaker.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Created by bayufa on 5/14/2015.
 */
public class Typefaces{
    private static String TAG = "TypefacesHelper";
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(Context c, String name){
        synchronized(cache){
            if(!cache.containsKey(name)){
                Typeface t = null;

                try {
                    t = Typeface.createFromAsset(c.getAssets(), String.format("fonts/%s", name));
                    if (t==null) {
                        Log.e(TAG, "typeface " + name + " not found in assets, use Default!!!");
                        t = Typeface.DEFAULT;
                    }
                } catch (Exception e) {
                    Log.e(TAG, "exception: font " + name + " not found in assets, use Default!!!");
                    t = Typeface.DEFAULT;
                }

                cache.put(name, t);
            }
            return cache.get(name);
        }
    }
}