package com.kodebonek.quotemaker.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.kodebonek.quotemaker.utils.Helper;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.Locale;

public class QuoteDB extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "quotes.db";
    private static final int DATABASE_VERSION = 1;  //first version
    private static final String TABLE_NAME = "quotedata";
    private static final String TAG = "QuoteDB";

    private ArrayList<String> categories;

    public QuoteDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        categories = new ArrayList<>();
    }

    public Cursor getQoutes(String filter,String category,boolean shuffle) {
        SQLiteDatabase db = this.getReadableDatabase();

        String queryFilter = "1=1";
        if (filter!=null && filter.length()>0)
            queryFilter += " AND message LIKE '%"+filter+"%'";
        if (category!=null && category.length()>0)
            queryFilter += " AND category='"+category.toLowerCase(Locale.getDefault())+"'";

        String orderby = "";
        if (shuffle) orderby = " ORDER BY random() LIMIT 100";

        String sql = "SELECT * FROM "+TABLE_NAME+" WHERE "+queryFilter+ orderby;
        Log.d(TAG, "query => " + sql);

        try {
            Cursor cursor = db.rawQuery(sql,null);
            return cursor;
        } catch (Exception e) {
            Log.e(TAG, "sql exception: " + e.getMessage());
            return null;
        }
    }

    public ArrayList<String> getCategories() {
        if (categories!=null && categories.size()>0)
            return categories;

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT category FROM "+TABLE_NAME+" GROUP BY category ORDER BY category ASC";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql,null);
        } catch (Exception e) {
            Log.e(TAG, "sql exception: " + e.getMessage());
            return null;
        }
        if (cursor==null) return null;

        if (cursor.moveToFirst()) {
            do {
                String category = Helper.capitalize(cursor.getString(0));
                categories.add(category);
            } while (cursor.moveToNext());

            return categories;
        } else {
            //category not found?
            return null;
        }
    }

    public Cursor getQuote(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" WHERE id="+id;
        try {
            Cursor cursor = db.rawQuery(sql, null);
            return cursor;
        } catch (Exception e) {
            Log.e(TAG, "sql exception: " + e.getMessage());
            return null;
        }
    }

    public boolean updateFavourite(int id,boolean faved) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql;
        if (faved)
            sql = "UPDATE "+TABLE_NAME+" SET favorite=1 WHERE id="+id;
        else
            sql = "UPDATE "+TABLE_NAME+" SET favorite=0 WHERE id="+id;

        try {
            Log.d(TAG, sql);
            db.execSQL(sql);
        } catch (SQLException e) {
            Log.e(TAG,"sq; exception: "+e.getMessage());
            return false;
        }

        return true;
    }

    public Cursor getFavourites() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" WHERE favorite=1";
        try {
            Cursor cursor = db.rawQuery(sql,null);
            return cursor;
        } catch (Exception e) {
            Log.e(TAG, "sql exception: " + e.getMessage());
            return null;
        }
    }
}
