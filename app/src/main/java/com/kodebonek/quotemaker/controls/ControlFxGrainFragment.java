package com.kodebonek.quotemaker.controls;

import android.media.effect.EffectFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.R;

public class ControlFxGrainFragment extends Fragment {

    private static final String TAG = "fxgrainfragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_seekbar_confirm, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        final Button btnApply = (Button) view.findViewById(R.id.btnApply);
        btnApply.setAlpha(.3f);

        if (((EditorActivity)getActivity()).fxOpen(EffectFactory.EFFECT_GRAIN)==false) {
            container.setVisibility(View.INVISIBLE);
            return;
        }

        seekBar.setMax(100);
        seekBar.setProgress(0);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    float level = (float) progress / seekBar.getMax();  //0..1
                    btnApply.setAlpha(1f);
                    ((EditorActivity) getActivity()).fxUpdateGrain(level);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditorActivity) getActivity()).fxApply();
                btnApply.setAlpha(.3f);
            }
        });
    }

}
