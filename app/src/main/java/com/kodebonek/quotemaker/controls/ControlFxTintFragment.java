package com.kodebonek.quotemaker.controls;

import android.graphics.Color;
import android.media.effect.EffectFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

import java.util.Arrays;
import java.util.List;

import at.markushi.ui.CircleButton;

public class ControlFxTintFragment extends Fragment {

    private static final String TAG = "fxtintfragment";
    private List<String> colors;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_color_confirm, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);
        final Button btnApply = (Button) view.findViewById(R.id.btnApply);
        btnApply.setAlpha(.3f);

        ClickColorListener listener = new ClickColorListener();
        listener.setApplyButton(btnApply);

        if (((EditorActivity)getActivity()).fxOpen(EffectFactory.EFFECT_TINT)==false) {
            container.setVisibility(View.INVISIBLE);
            return;
        }


        int dip = Helper.dipToPixel(getActivity(),50);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(dip,dip);

        colors = Arrays.asList(getActivity().getResources().getStringArray(R.array.color_palette));
        for(String hex : colors) {
            int c = Color.parseColor(hex);

            CircleButton btn = new CircleButton(getActivity());
            btn.setColor(c);
            btn.setTag(c);
            btn.setLayoutParams(lp);
            btn.setOnClickListener(listener);

            container.addView(btn);
        }

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditorActivity) getActivity()).fxApply();
                btnApply.setAlpha(.3f);
            }
        });
    }

    private class ClickColorListener implements View.OnClickListener {
        Button btnApply = null;

        public void setApplyButton(Button btn) {
            this.btnApply = btn;
        }

        @Override
        public void onClick(View v) {
            CircleButton btn = (CircleButton) v;
            int color = (int) btn.getTag();

            if (btnApply!=null)
                btnApply.setAlpha(1f);
            ((EditorActivity) getActivity()).fxUpdateTint(color);
        }
    }
}
