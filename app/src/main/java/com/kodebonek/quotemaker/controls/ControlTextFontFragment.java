package com.kodebonek.quotemaker.controls;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.R;
import com.kodebonek.quotemaker.utils.Typefaces;

import java.util.Arrays;
import java.util.List;

public class ControlTextFontFragment extends Fragment {

    private static final String TAG = "fontfragment";
    private static List<String> fonts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_font, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);
        ClickFontListener listener = new ClickFontListener();
        fonts = Arrays.asList(getActivity().getResources().getStringArray(R.array.quote_font));

        for (String font : fonts) {
            TextView tv = new TextView(getActivity());
            tv.setText("Quote");
            tv.setTextSize(36);
            if (!font.equalsIgnoreCase("default")) {
                Typeface face = Typefaces.get(getActivity(),font);
                tv.setTypeface(face);
            }
            tv.setPadding(0,0,20,0);
            tv.setClickable(true);
            tv.setCursorVisible(false);
            tv.setOnClickListener(listener);
            tv.setTag(font);

            container.addView(tv);
        }
    }

    private class ClickFontListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            TextView tv = (TextView) v;
            String font = (String) tv.getTag();
            //Toast.makeText(getActivity(), font, Toast.LENGTH_SHORT).show();

            ((EditorActivity) getActivity()).changeTextFont(font);
        }
    }
}
