package com.kodebonek.quotemaker.controls;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

import java.util.Arrays;
import java.util.List;

import at.markushi.ui.CircleButton;

public class ControlColorFragment extends Fragment {

    private static final String TAG = "colorfragment";
    private List<String> colors;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_color, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);

        ClickColorListener listener = new ClickColorListener();

        int dip = Helper.dipToPixel(getActivity(),50);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(dip,dip);

        colors = Arrays.asList(getActivity().getResources().getStringArray(R.array.color_palette));
        for(String hex : colors) {
            int c = Color.parseColor(hex);

            CircleButton btn = new CircleButton(getActivity());
            btn.setColor(c);
            btn.setTag(c);
            btn.setLayoutParams(lp);
            btn.setOnClickListener(listener);

            container.addView(btn);
        }

/*
        int num_colors = 30;
        float[] hsv = new float[3];
        for(int i = 0; i < 360; i += 360 / num_colors) {
            hsv[0] = i;
            hsv[1] = 90;
            hsv[2] = 50;
            int c = Color.HSVToColor(hsv);
        }
*/
    }

    private class ClickColorListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            CircleButton btn = (CircleButton) v;
            int color = (int) btn.getTag();

            ((EditorActivity) getActivity()).changeBackground(color);
        }
    }
}
