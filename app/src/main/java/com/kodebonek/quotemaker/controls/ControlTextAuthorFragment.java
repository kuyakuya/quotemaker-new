package com.kodebonek.quotemaker.controls;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

public class ControlTextAuthorFragment extends Fragment {

    private static final String TAG = "textauthorfragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_author, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);

        Button btnAuthorEdit = (Button) view.findViewById(R.id.btnAuthorEdit);
        Helper.styleButton(btnAuthorEdit);
        Button btnAuthorRemove = (Button) view.findViewById(R.id.btnAuthorRemove);
        Helper.styleButton(btnAuthorRemove);

        btnAuthorEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String current = ((EditorActivity)getActivity()).getAuthor();
                final EditText etName = new EditText(getActivity());
                etName.setText(current);

                FrameLayout fl = new FrameLayout(getActivity());
                int px = Helper.dipToPixel(getActivity(),20);
                fl.setPadding(px, px, px, px);
                fl.addView(etName);

                AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setTitle("Change Author Name")
                        .setView(fl)
                        .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = etName.getText().toString();
                                ((EditorActivity) getActivity()).changeAuthor(name);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                dialog = builder.create();
                dialog.getWindow().setSoftInputMode (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                dialog.show();
            }
        });

        btnAuthorRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditorActivity) getActivity()).changeAuthor("");
            }
        });
    }

}
