package com.kodebonek.quotemaker.controls;

import android.media.effect.EffectFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.R;

public class ControlFxTempFragment extends Fragment {

    private static final String TAG = "fxtempfragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_seekbar_confirm, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        final Button btnApply = (Button) view.findViewById(R.id.btnApply);
        btnApply.setAlpha(.3f);

        if (((EditorActivity)getActivity()).fxOpen(EffectFactory.EFFECT_TEMPERATURE)==false) {
            container.setVisibility(View.INVISIBLE);
            return;
        }

        //Float, between 0 and 1, with 0 indicating cool, and 1 indicating warm. A value of of 0.5 indicates no change.
        seekBar.setMax(100);
        seekBar.setProgress(50);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    float level = (float) progress / seekBar.getMax();
                    btnApply.setAlpha(1f);
                    ((EditorActivity) getActivity()).fxUpdateTemperature(level);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditorActivity) getActivity()).fxApply();
                btnApply.setAlpha(.3f);
            }
        });
    }

}
