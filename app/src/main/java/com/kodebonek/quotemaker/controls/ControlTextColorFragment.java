package com.kodebonek.quotemaker.controls;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

import java.util.Arrays;
import java.util.List;

import at.markushi.ui.CircleButton;

public class ControlTextColorFragment extends Fragment {

    private static final String TAG = "fontcolorfragment";
    private List<String> colors;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_color, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);
        ClickFontListener listener = new ClickFontListener();

        int dip = Helper.dipToPixel(getActivity(),50);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(dip,dip);

        colors = Arrays.asList(getActivity().getResources().getStringArray(R.array.color_palette));
        for(String hex : colors) {
            int c = Color.parseColor(hex);

            CircleButton btn = new CircleButton(getActivity());
            btn.setColor(c);
            btn.setTag(c);
            btn.setLayoutParams(lp);
            btn.setOnClickListener(listener);

            container.addView(btn);
        }
    }

    private class ClickFontListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            CircleButton btn = (CircleButton) v;
            int color = (int) btn.getTag();

            ((EditorActivity) getActivity()).changeTextColor(color);
        }
    }
}
