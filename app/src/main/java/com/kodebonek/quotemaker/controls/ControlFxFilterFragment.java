package com.kodebonek.quotemaker.controls;

import android.media.effect.EffectFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

public class ControlFxFilterFragment extends Fragment {

    private static final String TAG = "fxfilterfragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_filter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);

        Button btnSepia = (Button) view.findViewById(R.id.btnSepia); Helper.styleButton(btnSepia);
        Button btnCrossProcess = (Button) view.findViewById(R.id.btnCrossProcess); Helper.styleButton(btnCrossProcess);
        Button btnDocumentary = (Button) view.findViewById(R.id.btnDocumentary); Helper.styleButton(btnDocumentary);
        Button btnGrayscale = (Button) view.findViewById(R.id.btnGrayscale); Helper.styleButton(btnGrayscale);
        Button btnLomo = (Button) view.findViewById(R.id.btnLomo); Helper.styleButton(btnLomo);
        Button btnPosterize = (Button) view.findViewById(R.id.btnPosterize); Helper.styleButton(btnPosterize);
        Button btnNegative = (Button) view.findViewById(R.id.btnNegative); Helper.styleButton(btnNegative);

        StartFilterControl handler = new StartFilterControl();
        btnSepia.setOnClickListener(handler);
        btnCrossProcess.setOnClickListener(handler);
        btnDocumentary.setOnClickListener(handler);
        btnGrayscale.setOnClickListener(handler);
        btnLomo.setOnClickListener(handler);
        btnPosterize.setOnClickListener(handler);
        btnNegative.setOnClickListener(handler);

        final Button btnApply = (Button) view.findViewById(R.id.btnApply);
        handler.setApplyButton(btnApply);
        btnApply.setAlpha(.3f);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditorActivity) getActivity()).fxApply();
                btnApply.setAlpha(.3f);
            }
        });
    }

    private class StartFilterControl implements View.OnClickListener {

        private Button btnApply;

        public void setApplyButton(Button btn) {
            this.btnApply = btn;
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            EditorActivity activity = (EditorActivity) getActivity();
            switch(id) {
                case R.id.btnSepia:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_SEPIA);
                    break;
                case R.id.btnCrossProcess:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_CROSSPROCESS);
                    break;
                case R.id.btnDocumentary:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_DOCUMENTARY);
                    break;
                case R.id.btnGrayscale:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_GRAYSCALE);
                    break;
                case R.id.btnLomo:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_LOMOISH);
                    break;
                case R.id.btnPosterize:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_POSTERIZE);
                    break;
                case R.id.btnNegative:
                    activity.fxUpdateFilter(EffectFactory.EFFECT_NEGATIVE);
                    break;
                default:
                    break;
            }
            btnApply.setAlpha(1f);
        }
    }
}
