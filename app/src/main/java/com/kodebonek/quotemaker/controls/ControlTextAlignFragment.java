package com.kodebonek.quotemaker.controls;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

public class ControlTextAlignFragment extends Fragment {

    private static final String TAG = "fontalignfragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_fontalign, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container = (LinearLayout) view.findViewById(R.id.control_container);

        Button btnAlignLeft = (Button) view.findViewById(R.id.btnAlignLeft);
        Helper.styleButton(btnAlignLeft);
        Button btnAlignCenter = (Button) view.findViewById(R.id.btnAlignCenter);
        Helper.styleButton(btnAlignCenter);
        Button btnAlignRight = (Button) view.findViewById(R.id.btnAlignRight);
        Helper.styleButton(btnAlignRight);

        StartTextAlignment handler = new StartTextAlignment();
        btnAlignLeft.setOnClickListener(handler);
        btnAlignCenter.setOnClickListener(handler);
        btnAlignRight.setOnClickListener(handler);
    }

    private class StartTextAlignment implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id==R.id.btnAlignLeft) {
                ((EditorActivity)getActivity()).changeTextAlignment(Gravity.LEFT);

            } else if (id==R.id.btnAlignCenter) {
                ((EditorActivity)getActivity()).changeTextAlignment(Gravity.CENTER);

            } else if (id==R.id.btnAlignRight) {
                ((EditorActivity)getActivity()).changeTextAlignment(Gravity.RIGHT);

            }
        }
    }
}
