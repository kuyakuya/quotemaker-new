package com.kodebonek.quotemaker.controls;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kodebonek.quotemaker.EditorActivity;
import com.kodebonek.quotemaker.utils.Helper;
import com.kodebonek.quotemaker.R;

import java.util.Arrays;
import java.util.List;

import at.markushi.ui.CircleButton;

public class ControlGradientFragment extends Fragment {

    private static final String TAG = "gradfragment";
    private int selectedColor1 = Color.WHITE;
    private int selectedColor2 = Color.WHITE;
    private List<String> colors;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_fragment_twocolor, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        LinearLayout container1 = (LinearLayout) view.findViewById(R.id.control_container1);
        LinearLayout container2 = (LinearLayout) view.findViewById(R.id.control_container2);

        ClickColorListener listener = new ClickColorListener();

        int dip = Helper.dipToPixel(getActivity(),40);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(dip,dip);

        colors = Arrays.asList(getActivity().getResources().getStringArray(R.array.color_palette));
        for(String hex : colors) {
            int c = Color.parseColor(hex);

            CircleButton btn1 = new CircleButton(getActivity());
            btn1.setColor(c);
            btn1.setTag(new ColorData(c,true)); //first color
            btn1.setLayoutParams(lp);
            btn1.setOnClickListener(listener);
            container1.addView(btn1);

            CircleButton btn2 = new CircleButton(getActivity());
            btn2.setColor(c);
            btn2.setTag(new ColorData(c, false));   //second color
            btn2.setLayoutParams(lp);
            btn2.setOnClickListener(listener);
            container2.addView(btn2);
        }
    }


    private class ClickColorListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            CircleButton btn = (CircleButton) v;
            ColorData cd = (ColorData) btn.getTag();

            if (cd.first) selectedColor1 = cd.color;
            else selectedColor2 = cd.color;

            ((EditorActivity) getActivity()).changeBackground(selectedColor1,selectedColor2);
        }
    }

    private class ColorData {
        public int color;
        public boolean first;  //first or second

        ColorData(int color,boolean first) {
            this.color = color;
            this.first = first;
        }
    }
}
