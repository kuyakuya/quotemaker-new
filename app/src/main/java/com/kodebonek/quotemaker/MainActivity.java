package com.kodebonek.quotemaker;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.kodebonek.quotemaker.utils.AndroidPermissions;
import com.kodebonek.quotemaker.utils.Helper;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private MainPagerAdapter mainPagerAdapter;
    private static final int PERMISSIONS_REQUEST_CODE = 1;
    private AndroidPermissions mPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        mPermissions = new AndroidPermissions(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (!mPermissions.checkPermissions()) {
            mPermissions.requestPermissions(PERMISSIONS_REQUEST_CODE);

        } else {
            //permission OK
            setupPager();
        }

        //lock on portrait mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.i(TAG, "onRequestPermissionsResult");

        if (mPermissions.areAllRequiredPermissionsGranted(permissions, grantResults)) {
            setupPager();

        } else {
            onInsufficientPermissions();
        }

    }

    private void onInsufficientPermissions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This app is unable to work without being able to save quotes to phone storage." +
                "Please enable the access.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AndroidPermissions.startInstalledAppDetailsActivity(getApplicationContext());
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void setupPager() {

        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager_main);
        viewPager.setAdapter(mainPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs_main);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_rate) {
            Helper.openAppMarket(getApplicationContext());
        } else if (id == R.id.action_feedback) {
            Intent intent = new Intent(MainActivity.this,FeedbackActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void notifyNewQuote() {
        //new quote event from discover fragment / user create new quote from that place
        //refresh

        mainPagerAdapter.notifyNewQuote();

    }

    private class MainPagerAdapter extends FragmentPagerAdapter {

        private final int TABS_COUNT = 2;
        private String tabTitles[] = new String[] { "Your Quotes" , "Discover",};
        private YourQuoteFragment yourQuoteFragment = null;

        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    yourQuoteFragment = (YourQuoteFragment) YourQuoteFragment.newInstance(position);
                    return yourQuoteFragment;
                case 1:
                    return DiscoverFragment.newInstance(position);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return TABS_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public void notifyNewQuote() {
            if (yourQuoteFragment!=null) {
                yourQuoteFragment.reloadQuotes();
            }
        }
    }


}
