package com.kodebonek.quotemaker;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class FeedbackActivity extends AppCompatActivity {

    private static final String TAG = "FeedbackActivity";
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        setupToolbar();

        Spinner spin = (Spinner) findViewById(R.id.spinnerFeedback);
        spin.setAdapter(ArrayAdapter.createFromResource(this,
                R.array.menu_feedback,
                android.R.layout.simple_spinner_dropdown_item));

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Button btnSend = (Button) findViewById(R.id.buttonSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spinner = (Spinner) findViewById(R.id.spinnerFeedback);

                String name = "Anonymous";
                String type = spinner.getSelectedItem().toString();
                String message = ((EditText) findViewById(R.id.editText)).getText().toString();
                sendFeedback(name, type, message);
            }
        });

    }

    private void sendFeedback(String name, String type, String content) {
        //send the data
        String OS = Build.VERSION.RELEASE+ " (SDK: "+Build.VERSION.SDK_INT+")";
        String phone = Build.MANUFACTURER+" "+Build.MODEL;

        String URL = "http://api.kodebonek.com/quotemaker/feedback.php";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.add("app",getPackageName());
        params.add("os",OS);
        params.add("phone",phone);
        params.add("name",name);
        params.add("type", type);
        params.add("feedback", content);
        client.post(URL, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                setProgressVisible(true);
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {
                Toast.makeText(getApplicationContext(), "Thanks for your feedback. We'll process it ASAP", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, statusCode + " " + throwable.getMessage());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, statusCode + " " + throwable.getMessage());
            }

            @Override
            public void onFinish() {
                setProgressVisible(false);
                super.onFinish();
            }
        });
    }

    private void setProgressVisible(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void setupToolbar() {
        Button backButton = (Button) findViewById(R.id.btnBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
