package com.kodebonek.quotemaker;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kodebonek.quotemaker.sql.QuoteDB;
import com.kodebonek.quotemaker.utils.Helper;

import java.util.ArrayList;

public class DiscoverFragment extends Fragment {

    public static final String ARG_POSITION = "ARG_POSITION";
    private static final String TAG = "DiscoverFragment";
    private int mPosition;
    private QuoteDBAdapter mAdapter;
    private QuoteDB mDB;
    RecyclerView mRecyclerView;
    private TextView mEmptyView;

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);

        DiscoverFragment fragment = new DiscoverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //get context via getActivity()
        View view = inflater.inflate(R.layout.fragment_discover, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.quoteRecycleView);
        mEmptyView = (TextView) view.findViewById(R.id.tvEmptyView);
        LinearLayoutManager listManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);

        mDB = new QuoteDB(getActivity());

        mRecyclerView.setLayoutManager(listManager);
        mAdapter = new QuoteDBAdapter();
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                checkAdapterIsEmpty();
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        checkAdapterIsEmpty();

        Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        Helper.styleButton(btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FrameLayout fl = new FrameLayout(getActivity());
                int px = Helper.dipToPixel(getActivity(), 20);
                fl.setPadding(px, px, px, px);
                final EditText etSearchString = new EditText(getActivity());
                etSearchString.setHint("search here ...");
                fl.addView(etSearchString);

                AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setTitle("")
                        .setView(fl)
                        .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String str = etSearchString.getText().toString();
                                mEmptyView.setText("Quote '"+str+"' not found");
                                mAdapter.queryDB(str, null, false);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                dialog = builder.create();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                dialog.show();
            }
        });

        Button btnShuffle = (Button) view.findViewById(R.id.btnShuffle);
        Helper.styleButton(btnShuffle);
        btnShuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyView.setText("Quotes not found");
                mAdapter.queryDB(null, null, true);
            }
        });

        Button btnFilter = (Button) view.findViewById(R.id.btnFilter);
        Helper.styleButton(btnFilter);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayList<String> categories = mDB.getCategories();
                if (categories==null) {
                    Toast.makeText(getActivity(),"No categories found",Toast.LENGTH_SHORT).show();
                    return;
                }
                AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select a Category");
                builder.setItems(categories.toArray(new CharSequence[categories.size()]),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String selected = categories.get(which);
                                //Toast.makeText(getActivity(), selected, Toast.LENGTH_SHORT).show();
                                //Log.d(TAG, "selected: " + selected);
                                mEmptyView.setText("No quotes found");
                                mAdapter.queryDB(null,selected,false);
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });

        Button btnFavourite = (Button) view.findViewById(R.id.btnFavourite);
        Helper.styleButton(btnFavourite);
        btnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmptyView.setText("You have no favourite quote, yet");
                mAdapter.queryFavourite();
            }
        });

    }

    private void checkAdapterIsEmpty() {
        if (mAdapter.getItemCount()==0) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==YourQuoteFragment.REQUEST_CODE_NEW_QUOTE && resultCode== Activity.RESULT_OK) {
            String quoteID = data.getStringExtra("quoteID");
            if (quoteID.length()>0) {
                //Quote quote = Quote.newQuote(getActivity(),quoteID);
                //mAdapter.add(quote);
                //Toast.makeText(getActivity(), "Quote created. Please refresh to show the new quote", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).notifyNewQuote();
            }
        }
    }


    private class QuoteDBAdapter extends RecyclerView.Adapter {

        private ArrayList<QuoteDBEntry> quotes;

        QuoteDBAdapter() {
            quotes = new ArrayList<QuoteDBEntry>();
            queryDB(null, null, true);
        }

        public void queryDB(final String searchstr,final String category,final boolean shuffle) {

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    quotes.clear();
                    Cursor cursor = mDB.getQoutes(searchstr,category,shuffle);
                    if (cursor.moveToFirst()) {
                        do {
                            QuoteDBEntry entry = new QuoteDBEntry();
                            entry.ID = Integer.parseInt(cursor.getString(0));
                            entry.message = cursor.getString(1);
                            entry.author = cursor.getString(2);
                            entry.category = cursor.getString(3);
                            entry.faved = false;
                            if (Integer.parseInt(cursor.getString(4))==1)
                                entry.faved = true;
                            quotes.add(entry);

                        } while (cursor.moveToNext());
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    notifyDataSetChanged();
                }
            }.execute();
        }

        public void queryFavourite() {

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    quotes.clear();
                    Cursor cursor = mDB.getFavourites();
                    if (cursor.moveToFirst()) {
                        do {
                            QuoteDBEntry entry = new QuoteDBEntry();
                            entry.ID = Integer.parseInt(cursor.getString(0));
                            entry.message = cursor.getString(1);
                            entry.author = cursor.getString(2);
                            entry.category = cursor.getString(3);
                            entry.faved = false;
                            if (Integer.parseInt(cursor.getString(4))==1)
                                entry.faved = true;
                            quotes.add(entry);

                        } while (cursor.moveToNext());
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    notifyDataSetChanged();
                }
            }.execute();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_quotedb, viewGroup, false);

            QuoteViewHolder viewHolder = new QuoteViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            QuoteViewHolder holder = (QuoteViewHolder) viewHolder;
            QuoteDBEntry quote = quotes.get(position);
            if (quote==null) {
                Log.w(TAG, "quote is null !!!");
                return;
            }

            if (quote.faved) {
                holder.btnFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_small_faved));
            } else {
                holder.btnFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_small_fave));
            }
            holder.btnFave.setTag(quote.faved);

            ClickListener clickListener = new ClickListener(quote.ID);
            holder.btnRequote.setOnClickListener(clickListener);
            holder.btnFave.setOnClickListener(clickListener);
            holder.btnCopy.setOnClickListener(clickListener);
            holder.btnShare.setOnClickListener(clickListener);

            holder.tvQuote.setText(quote.message);
            holder.tvAuthor.setText(quote.author);
        }

        @Override
        public int getItemCount() {
            return quotes.size();
        }

        private QuoteDBEntry getQuote(int ID) {
            Cursor cursor = mDB.getQuote(ID);
            if (cursor==null) return null;

            if (cursor.moveToFirst()) {
                QuoteDBEntry entry = new QuoteDBEntry();
                entry.ID = Integer.parseInt(cursor.getString(0));
                entry.message = cursor.getString(1);
                entry.author = cursor.getString(2);
                entry.category = cursor.getString(3);
                entry.faved = false;
                if (Integer.parseInt(cursor.getString(4))==1)
                    entry.faved = true;

                return entry;
            } else
                return null;
        }

        private class ClickListener implements View.OnClickListener {

            private int mQuoteDbId;
            public ClickListener(int id) {
                this.mQuoteDbId = id;
            }

            @Override
            public void onClick(View v) {
                int resID = v.getId();

                boolean found = false;
                QuoteDBEntry entry = null;
                int pos;
                for (pos = 0; pos < quotes.size(); pos++) {
                    entry =  quotes.get(pos);
                    if (entry.ID==mQuoteDbId) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    Toast.makeText(getActivity(), "Oops, quote not found. Please refresh the app", Toast.LENGTH_SHORT).show();
                    return;
                }

                //QuoteDBEntry entry = getQuote(mQuoteDbId);

                if (resID==R.id.btnRequote) {

                    Intent myIntent = new Intent(getActivity(), EditorActivity.class);
                    //myIntent.putExtra("quoteID", "");
                    myIntent.putExtra("quoteAuthor", entry.author);
                    myIntent.putExtra("quoteMessage", entry.message);
                    startActivityForResult(myIntent, YourQuoteFragment.REQUEST_CODE_NEW_QUOTE);

                } else if (resID==R.id.btnFave) {
                    entry.faved = !entry.faved;
                    mDB.updateFavourite(mQuoteDbId, entry.faved);
                    if (pos>=0)
                        notifyItemChanged(pos);

                } else if (resID==R.id.btnShare) {
                    String textToShare = entry.message+"\n\n"+entry.author;

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/*");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
                    startActivity(Intent.createChooser(shareIntent, "Share Quote"));

                } else if (resID==R.id.btnCopy) {
                    String textToCopied = entry.message+"\n\n"+entry.author;

                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(getActivity().CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Quote",textToCopied);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getActivity(),"Quote text copied !",Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    private class QuoteDBEntry {
        public String message;
        public String author;
        public String category;
        public boolean faved;
        public int ID;
    }

    private class QuoteViewHolder extends RecyclerView.ViewHolder {
        protected TextView tvQuote;
        protected TextView tvAuthor;
        protected ImageButton btnRequote;
        protected ImageButton btnFave;
        protected ImageButton btnShare;
        protected ImageButton btnCopy;

        public QuoteViewHolder(View view) {
            super(view);
            this.tvQuote = (TextView) view.findViewById(R.id.tvMessage);
            this.tvAuthor = (TextView) view.findViewById(R.id.tvAuthor);
            this.btnRequote = (ImageButton) view.findViewById(R.id.btnRequote);
            this.btnFave = (ImageButton) view.findViewById(R.id.btnFave);
            this.btnShare = (ImageButton) view.findViewById(R.id.btnShare);
            this.btnCopy = (ImageButton) view.findViewById(R.id.btnCopy);
        }
    }

}
