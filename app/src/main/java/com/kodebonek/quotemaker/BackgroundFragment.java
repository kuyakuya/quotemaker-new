package com.kodebonek.quotemaker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.kodebonek.quotemaker.controls.ControlColorFragment;
import com.kodebonek.quotemaker.controls.ControlGradientFragment;
import com.kodebonek.quotemaker.utils.Helper;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.List;

public class BackgroundFragment extends Fragment {

    public static final String ARG_POSITION = "ARG_POSITION";
    private static final String TAG = "BackgroundFragment";
    private int mPosition;
    private Uri capturedURI;
    private int deviceWidth;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_FROM_GALLERY = 2;

    ControlColorFragment mControlColorFragment = null;
    ControlGradientFragment mControlGradientFragment = null;
    Button lastClickedButton = null;
    FrameLayout flControlContainer = null;
    PleaseSelectFragment mPleaseSelectFragment  = null;

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);

        BackgroundFragment fragment = new BackgroundFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);

        deviceWidth = Helper.getDeviceWidth(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //get context via getActivity()
        View view = inflater.inflate(R.layout.fragment_background, container, false);

        Button btnCamera = (Button) view.findViewById(R.id.btnCamera);
        Helper.styleButton(btnCamera);
        Button btnGallery = (Button) view.findViewById(R.id.btnGallery);
        Helper.styleButton(btnGallery);
        Button btnColor = (Button) view.findViewById(R.id.btnColor);
        Helper.styleButton(btnColor);
        Button btnGradient = (Button) view.findViewById(R.id.btnGradient);
        Helper.styleButton(btnGradient);

        flControlContainer = (FrameLayout) view.findViewById(R.id.control_fragment);

        btnCamera.setOnClickListener(new startCamera());
        btnGallery.setOnClickListener(new startGallery());
        btnColor.setOnClickListener(new startColor());
        btnGradient.setOnClickListener(new startGradient());

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        clearSelectedViews();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode==REQUEST_IMAGE_CAPTURE && resultCode==Activity.RESULT_OK) {
            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            getActivity().getContentResolver().notifyChange(capturedURI, null);
            Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
            Crop.of(capturedURI,destination).asSquare().
                    withMaxSize(deviceWidth, deviceWidth).
                    start(getActivity(), this);

        } else if (requestCode==REQUEST_IMAGE_FROM_GALLERY && resultCode==Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
            Crop.of(selectedImageUri,destination).asSquare().
                    withMaxSize(deviceWidth, deviceWidth).
                    start(getActivity(),this);

        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleCrop(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            ((EditorActivity) getActivity()).changeBackground(Crop.getOutput(data));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(data).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private class startCamera implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            File captured;
            try {
                captured = Helper.createTemporaryFile("captured",".jpg");
                captured.delete();
            } catch (Exception e) {
                Log.e(TAG, "createTemporaryFile failed: " + e.getMessage());
                Toast.makeText(getActivity(),"Prepare temp file failed! please check your SD card",Toast.LENGTH_SHORT).show();
                return;
            }

            capturedURI = Uri.fromFile(captured);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedURI);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } else {
                Toast.makeText(getActivity(),"Unable to start camera",Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Unable to start camera");
            }

            clearSelectedViews();
        }
    }

    private class startGallery implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select a Picture"), REQUEST_IMAGE_FROM_GALLERY);

            clearSelectedViews();
        }
    }

    private class startPattern implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            //create list of color button
        }
    }

    private class startColor implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlColorFragment==null) {
                mControlColorFragment = new ControlColorFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlColorFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private class startGradient implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mControlGradientFragment==null) {
                mControlGradientFragment = new ControlGradientFragment();
            }

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.control_fragment, mControlGradientFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }

    private void highlightSelectedViews(View v) {

        if (lastClickedButton!=null) {
            //restore to normal
            lastClickedButton.setBackgroundColor(Color.TRANSPARENT);
        }

        Button btn = (Button)v;
        btn.setBackgroundColor(Color.parseColor("#DDDDDD"));
        flControlContainer.setBackgroundColor(Color.parseColor("#DDDDDD"));
        lastClickedButton = btn;
    }

    private void clearSelectedViews() {
        List<Fragment> al = getChildFragmentManager().getFragments();
        if (al != null) {
            for (Fragment frag : al) {
                getChildFragmentManager().beginTransaction().remove(frag).commit();
            }
        }

        flControlContainer.setBackgroundColor(Color.TRANSPARENT);
        if (lastClickedButton!=null) {
            lastClickedButton.setBackgroundColor(Color.TRANSPARENT);
            lastClickedButton = null;
        }

        if (mPleaseSelectFragment==null) {
            mPleaseSelectFragment = new PleaseSelectFragment();
        }

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.control_fragment, mPleaseSelectFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}
