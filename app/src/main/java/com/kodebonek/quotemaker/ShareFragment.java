package com.kodebonek.quotemaker;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.kodebonek.quotemaker.model.QuoteEvent;
import com.kodebonek.quotemaker.utils.Helper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import de.greenrobot.event.EventBus;

public class ShareFragment extends Fragment {

    public static final String ARG_POSITION = "ARG_POSITION";
    private static final String TAG = "ShareFragment";
    private int mPosition;
    private int mClickedButtonId;

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);

        ShareFragment fragment = new ShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"onCreate()");

        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public void onStart() {
        Log.d(TAG,"onStart()");
        if (EventBus.getDefault().isRegistered(this)==false)
            EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onPause() {
        Log.d(TAG,"onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG,"onStop()");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //get context via getActivity()

        View view = inflater.inflate(R.layout.fragment_share, container, false);
        Button btnSave = (Button) view.findViewById(R.id.btnSave);
        Helper.styleButton(btnSave);
        Button btnInstagram = (Button) view.findViewById(R.id.btnInstagram);
        Helper.styleButton(btnInstagram);
        Button btnFacebook = (Button) view.findViewById(R.id.btnFacebook);
        Helper.styleButton(btnFacebook);
        Button btnShareOther = (Button) view.findViewById(R.id.btnShareOther);
        Helper.styleButton(btnShareOther);

        ClickListener clickListener = new ClickListener();
        btnSave.setOnClickListener(clickListener);
        btnInstagram.setOnClickListener(clickListener);
        btnFacebook.setOnClickListener(clickListener);
        btnShareOther.setOnClickListener(clickListener);

        return view;
    }

    public void onEvent(QuoteEvent event) {
        int event_type = event.type;
        if (event_type == QuoteEvent.SAVED_SUCCESS) {
            Log.d(TAG, "received event: SAVED_SUCCESS. Uri:"+ event.savedImageUri.getPath());
            processSharing(mClickedButtonId,event.savedImageUri);

        } else if (event_type == QuoteEvent.SAVED_FAILED) {
            Log.d(TAG, "received event: SAVED_FAILED. Reason:"+event.failReason);

        } else if (event_type == QuoteEvent.IMAGE_EXIST) {
            Log.d(TAG, "received event: IMAGE_EXIST. Uri:"+event.savedImageUri.getPath());
            processSharing(mClickedButtonId,event.savedImageUri);
        }
    }

    private void processSharing(int buttonID, Uri thumbUri) {

        //Log.d(TAG,"orig image uri for sharing: "+thumbUri.getPath());
        Uri uri = FileProvider.getUriForFile(getActivity(), "com.kodebonek.quotemaker.EditorActivity", new File(thumbUri.getPath()));
        Log.d(TAG,"new image uri for sharing: "+uri.getPath());

        if (buttonID==R.id.btnSave) {
            //Toast.makeText(getActivity(),"save",Toast.LENGTH_SHORT).show();
            File src = new File(thumbUri.getPath());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
            Calendar cal = Calendar.getInstance();

            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            String appname = getResources().getString(R.string.app_name);
            File myDir = new File(root + "/"+ appname);
            myDir.mkdirs();

            String fname = "myquote-" + dateFormat.format(cal.getTime()) + ".png";
            File dst = new File(myDir, fname);

            try {
                Helper.copyFile(src,dst);

                // Tell the media scanner about the new file so that it is immediately available to the user.
                MediaScannerConnection.scanFile(getActivity(), new String[]{dst.toString()}, null, null);
                Toast.makeText(getActivity(),"Quote copied to your phone",Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                Log.e(TAG,"copyFile excetion: "+e.toString());
                Toast.makeText(getActivity(),"Copy image failed.",Toast.LENGTH_SHORT).show();
            }

        } else if (buttonID==R.id.btnInstagram) {
            String packageName = "com.instagram.android";
            Intent chooser = Helper.createChooserIntent(getActivity(),packageName,"image/*",
                    "Check my quote!","created with BeWise android app",uri);
            if (chooser==null) {
                Toast.makeText(getActivity(),"Instagram app not installed?",Toast.LENGTH_SHORT).show();
            } else {
                getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(chooser);
                getActivity().revokeUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

        } else if (buttonID==R.id.btnFacebook) {
            String packageName = "com.facebook.katana";
            Intent chooser = Helper.createChooserIntent(getActivity(),packageName,"image/*",
                    "Check my quote!","created with BeWise android app",uri);
            if (chooser==null) {
                Toast.makeText(getActivity(),"Facebook app not installed?",Toast.LENGTH_SHORT).show();
            } else {
                getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(chooser);
                getActivity().revokeUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

        } else if (buttonID==R.id.btnShareOther) {

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            //grant permisions for all apps that can handle given intent
            List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

            startActivity(Intent.createChooser(shareIntent, "Share to other app"));
        }
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int ID = v.getId();
            mClickedButtonId = ID;

            Uri imageFile = ((EditorActivity) getActivity()).beginSharing();
        }
    }
}
