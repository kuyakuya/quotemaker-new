package com.kodebonek.quotemaker;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.kodebonek.quotemaker.controls.ControlFxAutoFragment;
import com.kodebonek.quotemaker.controls.ControlFxBrightnessFragment;
import com.kodebonek.quotemaker.controls.ControlFxContrastFragment;
import com.kodebonek.quotemaker.controls.ControlFxDuotoneFragment;
import com.kodebonek.quotemaker.controls.ControlFxFilllightFragment;
import com.kodebonek.quotemaker.controls.ControlFxFilterFragment;
import com.kodebonek.quotemaker.controls.ControlFxGrainFragment;
import com.kodebonek.quotemaker.controls.ControlFxSaturateFragment;
import com.kodebonek.quotemaker.controls.ControlFxTempFragment;
import com.kodebonek.quotemaker.controls.ControlFxTintFragment;
import com.kodebonek.quotemaker.controls.ControlFxVignetteFragment;
import com.kodebonek.quotemaker.utils.Helper;

import java.util.List;

/**
 * Created by bayufa on 6/6/2015.
 */
public class EffectFragment extends Fragment {

    public static final String ARG_POSITION = "ARG_POSITION";
    private int mPosition;
    Button lastClickedButton = null;
    FrameLayout flControlContainer = null;

    private ControlFxBrightnessFragment mControlFxBrightness;
    private ControlFxContrastFragment mControlFxContrast;
    private ControlFxAutoFragment mControlFxAuto;
    private ControlFxSaturateFragment mControlFxSaturate;
    private ControlFxTempFragment mControlFxTemp;
    private ControlFxTintFragment mControlFxTint;
    private ControlFxVignetteFragment mControlFxVignette;
    private ControlFxDuotoneFragment mControlFxDuotone;
    private ControlFxFilllightFragment mControlFxFilllight;
    private ControlFxGrainFragment mControlFxGrain;
    private ControlFxFilterFragment mControlFxFilter;
    private PleaseSelectFragment mPleaseSelectFragment;

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);

        EffectFragment fragment = new EffectFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //get context via getActivity()
        View view = inflater.inflate(R.layout.fragment_effects, container, false);
        flControlContainer = (FrameLayout) view.findViewById(R.id.control_fragment);

        Button btnFxFilter = (Button) view.findViewById(R.id.btnFxFilter); Helper.styleButton(btnFxFilter);
        Button btnFxAuto = (Button) view.findViewById(R.id.btnFxAuto); Helper.styleButton(btnFxAuto);
        Button btnFxBrightness = (Button) view.findViewById(R.id.btnFxBrightness); Helper.styleButton(btnFxBrightness);
        Button btnFxContrast = (Button) view.findViewById(R.id.btnFxContrast); Helper.styleButton(btnFxContrast);
        Button btnFxSaturate = (Button) view.findViewById(R.id.btnFxSaturate); Helper.styleButton(btnFxSaturate);
        Button btnFxTemperature = (Button) view.findViewById(R.id.btnFxTemperature); Helper.styleButton(btnFxTemperature);
        Button btnFxTint = (Button) view.findViewById(R.id.btnFxTint); Helper.styleButton(btnFxTint);
        Button btnFxVignette = (Button) view.findViewById(R.id.btnFxVignette); Helper.styleButton(btnFxVignette);
        Button btnFxDuotone = (Button) view.findViewById(R.id.btnFxDuotone); Helper.styleButton(btnFxDuotone);
        Button btnFxFilllight = (Button) view.findViewById(R.id.btnFxFilllight); Helper.styleButton(btnFxFilllight);
        Button btnFxGrain = (Button) view.findViewById(R.id.btnFxGrain); Helper.styleButton(btnFxGrain);

        StartFxControl handler = new StartFxControl();
        btnFxAuto.setOnClickListener(handler);
        btnFxBrightness.setOnClickListener(handler);
        btnFxContrast.setOnClickListener(handler);
        btnFxSaturate.setOnClickListener(handler);
        btnFxTemperature.setOnClickListener(handler);
        btnFxTint.setOnClickListener(handler);
        btnFxVignette.setOnClickListener(handler);
        btnFxDuotone.setOnClickListener(handler);
        btnFxFilllight.setOnClickListener(handler);
        btnFxGrain.setOnClickListener(handler);
        btnFxFilter.setOnClickListener(handler);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clearSelectedViews();
    }

    private void highlightSelectedViews(View v) {
        if (lastClickedButton!=null) {
            //restore to normal
            lastClickedButton.setBackgroundColor(Color.TRANSPARENT);
        }

        Button btn = (Button)v;
        btn.setBackgroundColor(Color.parseColor("#DDDDDD"));
        flControlContainer.setBackgroundColor(Color.parseColor("#DDDDDD"));
        lastClickedButton = btn;
    }

    private void clearSelectedViews() {
        List<Fragment> al = getChildFragmentManager().getFragments();
        if (al != null) {
            for (Fragment frag : al) {
                getChildFragmentManager().beginTransaction().remove(frag).commit();
            }
        }

        flControlContainer.setBackgroundColor(Color.TRANSPARENT);
        if (lastClickedButton!=null) {
            lastClickedButton.setBackgroundColor(Color.TRANSPARENT);
            lastClickedButton = null;
        }

        if (mPleaseSelectFragment==null) {
            mPleaseSelectFragment = new PleaseSelectFragment();
        }

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.control_fragment, mPleaseSelectFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private class StartFxControl implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

            if (id==R.id.btnFxAuto) {
                if (mControlFxAuto==null)
                    mControlFxAuto = new ControlFxAutoFragment();
                transaction.replace(R.id.control_fragment, mControlFxAuto);
            } else if (id==R.id.btnFxBrightness) {
                if (mControlFxBrightness==null)
                    mControlFxBrightness = new ControlFxBrightnessFragment();
                transaction.replace(R.id.control_fragment, mControlFxBrightness);
            } else if (id==R.id.btnFxContrast) {
                if (mControlFxContrast==null)
                    mControlFxContrast = new ControlFxContrastFragment();
                transaction.replace(R.id.control_fragment, mControlFxContrast);
            } else if (id==R.id.btnFxSaturate) {
                if (mControlFxSaturate==null)
                    mControlFxSaturate = new ControlFxSaturateFragment();
                transaction.replace(R.id.control_fragment, mControlFxSaturate);
            } else if (id==R.id.btnFxTemperature) {
                if (mControlFxTemp==null)
                    mControlFxTemp = new ControlFxTempFragment();
                transaction.replace(R.id.control_fragment, mControlFxTemp);
            } else if (id==R.id.btnFxTint) {
                if (mControlFxTint==null)
                    mControlFxTint = new ControlFxTintFragment();
                transaction.replace(R.id.control_fragment, mControlFxTint);
            } else if (id==R.id.btnFxVignette) {
                if (mControlFxVignette==null)
                    mControlFxVignette = new ControlFxVignetteFragment();
                transaction.replace(R.id.control_fragment, mControlFxVignette);
            } else if (id==R.id.btnFxDuotone) {
                if (mControlFxDuotone==null)
                    mControlFxDuotone = new ControlFxDuotoneFragment();
                transaction.replace(R.id.control_fragment, mControlFxDuotone);
            } else if (id==R.id.btnFxFilllight) {
                if (mControlFxFilllight==null)
                    mControlFxFilllight = new ControlFxFilllightFragment();
                transaction.replace(R.id.control_fragment, mControlFxFilllight);
            } else if (id==R.id.btnFxGrain) {
                if (mControlFxGrain==null)
                    mControlFxGrain = new ControlFxGrainFragment();
                transaction.replace(R.id.control_fragment, mControlFxGrain);
            } else if (id==R.id.btnFxFilter) {
                if (mControlFxFilter==null)
                    mControlFxFilter = new ControlFxFilterFragment();
                transaction.replace(R.id.control_fragment, mControlFxFilter);
            }

            transaction.addToBackStack(null);
            transaction.commit();

            highlightSelectedViews(v);
        }
    }
}
